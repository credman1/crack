#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "md5.h"

int main(int argc, char *argv[])
{
    // Check number of cmd line args
    if (argc < 3)
    {
        printf("Must supply a src and dest filename\n");
        exit(1);
    }
    
    // Open first file for reading
    FILE *src = fopen(argv[1], "r");
    if (!src)
    {
        printf("can't open %s for reading\n", argv[1]);
        exit(1);
    }
    
    // Open second file for writing
    FILE *dest = fopen(argv[2], "w");
    if (!dest)
    {
        printf("Can't open %s for writing.\n", argv[2]);
        exit(1);
    }
    
    // Loop through first file, hashing each line and writing to second file
    char pswrd[100];
    while (fgets(pswrd, 100, src) != NULL)
    {
        //printf("%lu\n", strlen(pswrd));
        char *hash = md5(pswrd, (strlen(pswrd)-1));
        
        fprintf(dest, "%s\n", hash);
        //printf("Password: %s - Hash: %s\n\n", pswrd, hash);
        free(hash);
    }
    
    //printf("Created file %s with hashes.\n", argv[2]);
    fclose(src);
    fclose(dest);
}